from werkzeug.security import check_password_hash,generate_password_hash
from flask_mongoengine import MongoEngine
import datetime

db = MongoEngine()

class User(db.Document):

    email = db.StringField(max_length=120)
    #_password = db.StringField(max_length=256)
    project = db.ListField(db.StringField(max_length=64))
    creation_date = db.DateTimeField()
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def __init__(self, *args, **kwargs):
        super(db.Document, self).__init__(*args, **kwargs)
        #self.login = db.StringField(max_length=80, unique=True)
        #self.email = db.StringField(max_length=120)
        self._password = db.StringField(max_length=256)
        #self.project = db.ListField(db.StringField(max_length=64))
        #self.creation_date = db.DateTimeField()
        #self.modified_date = db.DateTimeField(default=datetime.datetime.now)




    # Flask-Login integration
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(id)

    @property
    def password(self):
        print("getter of x password called")
        return self._password

    @password.setter
    def password(self, value):
        self._password = generate_password_hash(value)

    @staticmethod
    def validate_login(password_hash, password):
        return check_password_hash(password_hash, password)


class Project(db.Document):

    use_case = db.StringField(max_length=64)
    data_source = db.StringField(max_length=300)
    creation_date = db.DateTimeField()
    modified_date = db.DateTimeField(default=datetime.datetime.now)

    def get_id(self):
        return str(self.id)
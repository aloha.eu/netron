# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g

from . import Resource
from .. import schemas
import netron, os
SHARED_DATA_FOLDER = "/opt/data/"

class NetronStart(Resource):
        
    def post(self):
        print(g.args)
        onnx_path = g.args.get ("onnx_path")
        onnx_path= os.path.join(SHARED_DATA_FOLDER, onnx_path)
        print (onnx_path)
        netron.start(onnx_path, browse=False, port=5555, host='0.0.0.0')

        return {'message': 'something', 'job_id': 'something'}, 200, None

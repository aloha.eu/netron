# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
### 
### The code is auto generated, your change will be overwritten by 
### code generating.
###
from __future__ import absolute_import

from .api.netron_start import NetronStart


routes = [
    dict(resource=NetronStart, urls=['/netron_start'], endpoint='netron_start'),
]